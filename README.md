# AProjectThatMakesTextScrollEndlesslyOnAnOLEDAndAlsoPlaysMusicAllControlledFromASingleArduinoNano

In this project we'll playing around with sounds and graphics through an 8ohm speaker and 1306 controlled OLED,
controlled from an Arduino Nano.

## CLI Serial Monitor Usage
Make the script executable with `sudo chmod +x serialMonitor.py`

Then enter the following
* Change your port and baud rate as required
```
python3 serialMonitor.py --port /dev/ttyUSB0 --baud 9600
```

## Arduino CLI Example

* Install the CLI
* Compile your .ino program 
    * My arduino nano uses old bootloader so need fbqn looks a bit different
```
arduino-cli compile --fqbn arduino:avr:nano:cpu=atmega328old
```
* Upload your compiled program
```
arduino-cli upload -p /dev/ttyUSB0 --fqbn  arduino:avr:nano:cpu=atmega328old
```



