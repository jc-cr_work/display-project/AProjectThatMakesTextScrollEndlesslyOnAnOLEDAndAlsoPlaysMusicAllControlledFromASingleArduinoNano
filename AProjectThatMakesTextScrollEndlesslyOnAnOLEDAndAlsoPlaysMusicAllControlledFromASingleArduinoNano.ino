#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128 // OLED display width
#define SCREEN_HEIGHT 32 // OLED display height

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire);

void setup() {
  // initialize with the I2C addr 0x3C (for the 128x32)
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    // If initialization failed, you might want to check the wiring and the I2C address of your module.
    for(;;); // Don't proceed, loop forever
  }

  display.display();
  delay(2000);

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Hello, World!"); // Change this to your desired message.
  display.display();
}


int main(void)
{

  init(); // Must intialize the arduino firmware
  setup();

  while(1)
  {
    display.startscrollright(0x00, 0x0F);
  }

  return 0;
}



