import serial
import argparse
import threading
import queue
import time

# Argument parser to handle command line arguments
parser = argparse.ArgumentParser(description="Simple Serial Monitor")
parser.add_argument('--port', metavar='P', type=str, help='the port to listen to')
parser.add_argument('--baud', metavar='B', type=int, help='the baud rate')
args = parser.parse_args()

# Configure serial port
ser = serial.Serial(port=args.port, baudrate=args.baud)
print(f"Connected to {args.port} at {args.baud} baud rate.")

# Use a queue to store input from the user
input_queue = queue.Queue()


# Define a function for a thread to get input
def input_thread(q):
    while True:
        q.put(input("Enter message: "))
        time.sleep(0.1)


if __name__ == '__main__':

    # Start the input thread
    threading.Thread(target=input_thread, args=(input_queue,), daemon=True).start()

    try:
        while True:
            # Execute when a message is received
            if ser.in_waiting:
                # Read line from Arduino
                rxMsg = ser.readline().decode('utf-8').rstrip()
                print(rxMsg)

            # Send user input to nano
            if not input_queue.empty():
                txMsg = input_queue.get()
                ser.write((txMsg + '\n').encode('utf-8'))

    except KeyboardInterrupt:
        print("\nexiting...")
        ser.close()
